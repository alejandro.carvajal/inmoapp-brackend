# Backend-End Development Test

En el repositorio podrá encontrar el un proyecto de nombre "project_inmobilio" con la configuración del proyecto creaddo con el framework Django en lenguaje Python, así mismo encontrará una aplicación de python de nombre "api_inmobilio" donde se encuentran los modelos, las vistas, el enrutado (ulr's) y el serializador de los modelos del APIs REST.  

## Documentación

Antes de intentar ejecutar el proyecto lea muy bien la documentación del API, la cual se encuentra en la siguiente ruta:

* buil/html/index.html

En esta página html encontrará los requisitos de instalación de las librerías que necesita este proyecto parta poder ser ejecutado, así mismo encontrará parte del código usado en el proyecto.

## Base de datos

No utilicé una base de datos local de PostgreSQL, preferí crear una base de datos en línea en un servidor de pruebas libre en ElephantSQL, para que quedara pública y no tener que estar contruyendo la base de datos en local y evitar de pronto los errores entre versiones. 

La configuración de la cadena de conexión de la base de datos de encuentra en el archivo "settings.py" de la carpeta "project_inmobilio", en la cofiguración "DATABASES".