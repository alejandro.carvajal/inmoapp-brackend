.. Inmobilio documentation master file, created by
   sphinx-quickstart on Wed Oct 10 17:35:52 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vienvenido a la documentación del API de Inmobilio
==================================================

Esta es la documentación del API de inmobilio

Requerimientos para poder probar el API:

* Instalar el intérprete de Python localmente
* Adicionar la variable de entorno del Python: 
    Panel de control
        Sistema y Seguridad 
            Sistema 
                Configuración avanzada del sistema 
                    Variables de entorno 
                        Path += ";" + URL donde se encuentra el pythhon.exe
* Instalar el Django
* Instalar pip
* Instalar env: 
    py -3 -m venv env
* Instalar Django en el directorio virtual: 
    python -m pip install django
* Cambiar la ruta local del ejecutable de python:
    env
        pyvenv.cfg
            home = URL donde se encuentra el pythhon.exe
* Modificar el intérprete de pythhon en la conexión "Python: Django" del archivo launch.json así:
    launch.json
        Buscar configuración "name": "Python: Django"

        Cambiar el atributo "pythonPath": "URL donde se encuentra el pythhon.exe" (incluyendo python.exe)
* Instalar psycopg2: 
    pip install psycopg2
* Instalar rest_framework: 
    pip3 install djangorestframework

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   codigo

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
