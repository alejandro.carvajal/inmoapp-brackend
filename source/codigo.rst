Código del API Inmobilio
*********

Aquí podrá encontrar el código que se utilizó en la construcción del API

Modelos
=======

class General(models.Model):
    """
    Modelo encargado de representar el objeto General en la base de datos
    """
    direccion = models.CharField(max_length=300, null=False)
    ciudad = models.CharField(max_length=300, null=False)
    departamento = models.CharField(max_length=300, null=False)
    pais = models.CharField(max_length=300, null=False)
    telefono = models.CharField(max_length=300, null=False)

    class Meta:
        verbose_name = _("General")
        verbose_name_plural = _("Generales")

class Interior(models.Model):
    """
    Modelo encargado de representar el objeto Interior en la base de datos
    """
    cuartos = models.FloatField(null=False)
    banios = models.IntegerField(null=False)
    closets = models.IntegerField(null=False)
    calentador = models.NullBooleanField()

    class Meta:
        verbose_name = _("Interior")
        verbose_name_plural = _("Interiores")

class Exterior(models.Model):
    """
    Modelo encargado de representar el objeto Exterior en la base de datos
    """
    vigilancia = models.CharField(max_length=300, null=False)
    parqueadero = models.CharField(max_length=300, null=False)
    salon_social = models.NullBooleanField()
    numero_pisos = models.IntegerField(null=False)

    class Meta:
        verbose_name = _("Exterior")
        verbose_name_plural = _("Exteriores")

class Inmueble(models.Model):
    """
    Modelo encargado de representar el objeto Inmueble en la base de datos
    """
    tipo = models.CharField(max_length=300, null=False)
    subtipo = models.CharField(max_length=300, null=False)
    general = models.ForeignKey(General, on_delete=models.CASCADE)
    interior = models.ForeignKey(Interior, on_delete=models.CASCADE)
    exterior = models.ForeignKey(Exterior, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Inmueble")
        verbose_name_plural = _("Inmuebles")

Vistas
======

@api_view(['GET'])
def inmuebles(request):
    if request.method == 'GET':
        inmuebles = Inmueble.objects.all()
        serializer = InmuebleSerializer(inmuebles, many=True)
        return Response(serializer.data)

@api_view(['POST'])
def inmueble_create(request):
    if request.method == 'POST':
        serializer = InmuebleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def inmueble(request, id):
    try:
        inmueble = Inmueble.objects.get(id=id)
    except Inmueble.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = InmuebleSerializer(inmueble)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = InmuebleSerializer(inmueble, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        inmueble.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)