from django.contrib import admin
from api_inmobilio.models import Inmueble

# Register your models here.
admin.site.register(Inmueble)
