from rest_framework import status, serializers
from api_inmobilio.models import *

class InteriorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interior
        fields = ('cuartos', 'banios', 'closets', 'calentador')

class ExteriorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exterior
        fields = ('vigilancia', 'parqueadero', 'salon_social', 'numero_pisos')

class GeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = General
        fields = ('direccion', 'ciudad', 'departamento', 'pais', 'telefono')

class InmuebleSerializer(serializers.ModelSerializer):
    general = GeneralSerializer(required=True)
    interior = InteriorSerializer(required=True)
    exterior = ExteriorSerializer(required=True)

    class Meta:
        model = Inmueble
        fields = ('id', 'tipo', 'subtipo', 'general', 'interior', 'exterior')

    def create(self, validated_data):
        general_data = validated_data.pop('general')
        general = GeneralSerializer.create(GeneralSerializer(), validated_data=general_data)
        interior_data = validated_data.pop('interior')
        interior = InteriorSerializer.create(InteriorSerializer(), validated_data=interior_data)
        exterior_data = validated_data.pop('exterior')
        exterior = ExteriorSerializer.create(ExteriorSerializer(), validated_data=exterior_data)

        inmueble, created = Inmueble.objects.update_or_create(
            tipo = validated_data.pop('tipo'),
            subtipo = validated_data.pop('subtipo'),
            general = general,
            interior = interior,
            exterior = exterior
        )

        return inmueble