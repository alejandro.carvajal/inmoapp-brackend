from django.db import models
from django.utils.translation import gettext as _

class General(models.Model):
    """
    Modelo encargado de representar ell objeto General en la base de datos
    """
    direccion = models.CharField(max_length=300, null=False)
    ciudad = models.CharField(max_length=300, null=False)
    departamento = models.CharField(max_length=300, null=False)
    pais = models.CharField(max_length=300, null=False)
    telefono = models.CharField(max_length=300, null=False)

    class Meta:
        verbose_name = _("General")
        verbose_name_plural = _("Generales")

class Interior(models.Model):
    """
    Modelo encargado de representar ell objeto Interior en la base de datos
    """
    cuartos = models.FloatField(null=False)
    banios = models.IntegerField(null=False)
    closets = models.IntegerField(null=False)
    calentador = models.NullBooleanField()

    class Meta:
        verbose_name = _("Interior")
        verbose_name_plural = _("Interiores")

class Exterior(models.Model):
    """
    Modelo encargado de representar ell objeto Exterior en la base de datos
    """
    vigilancia = models.CharField(max_length=300, null=False)
    parqueadero = models.CharField(max_length=300, null=False)
    salon_social = models.NullBooleanField()
    numero_pisos = models.IntegerField(null=False)

    class Meta:
        verbose_name = _("Exterior")
        verbose_name_plural = _("Exteriores")

class Inmueble(models.Model):
    """
    Modelo encargado de representar ell objeto Inmueble en la base de datos
    """
    tipo = models.CharField(max_length=300, null=False)
    subtipo = models.CharField(max_length=300, null=False)
    general = models.ForeignKey(General, on_delete=models.CASCADE)
    interior = models.ForeignKey(Interior, on_delete=models.CASCADE)
    exterior = models.ForeignKey(Exterior, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Inmueble")
        verbose_name_plural = _("Inmuebles")
