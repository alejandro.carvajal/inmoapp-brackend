from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse
from django.shortcuts import render
from api_inmobilio.models import *
from api_inmobilio.serializer import *

import re

def documentation(request):
    return HttpResponse('Podrá encontrar la documentación de la API en la carpeta en la siguiente ruta "build/html/index.html"')

@api_view(['GET'])
def inmuebles(request):
    if request.method == 'GET':
        inmuebles = Inmueble.objects.all()
        serializer = InmuebleSerializer(inmuebles, many=True)
        return Response(serializer.data)

@api_view(['POST'])
def inmueble_create(request):
    if request.method == 'POST':
        serializer = InmuebleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def inmueble(request, id):
    try:
        inmueble = Inmueble.objects.get(id=id)
    except Inmueble.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = InmuebleSerializer(inmueble)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = InmuebleSerializer(inmueble, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        inmueble.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)