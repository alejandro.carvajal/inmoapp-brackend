from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.documentation),
    url(r'^inmuebles/$', views.inmuebles),
    url(r'^inmueble/(?P<id>[0-9]+)$', views.inmueble),
    url(r'^inmueble/$', views.inmueble_create),
]