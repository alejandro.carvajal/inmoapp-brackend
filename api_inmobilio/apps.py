from django.apps import AppConfig


class ApiInmobilioConfig(AppConfig):
    name = 'api_inmobilio'
