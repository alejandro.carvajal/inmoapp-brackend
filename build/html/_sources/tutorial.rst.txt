Tutorial API Inmobilio
*********

Ests es el tutorial para el consumo del API de Inmobilio

Los endpoints de consumo públicos del API son los siguientes:

* POST /inmueble para la creación de un nuevo inmueble con todas las características.
* PUT /inmueble/{id} para la actualización de un inmueble existente.
* DELETE /inmueble/{id} para la eliminación de un inmueble existente.
* GET /inmueble/{id} para imprimir en pantalla el json de un inmueble almacenado.
* GET /inmuebles para imprimir el listado total de inmuebles.

Para poder consumir los métodos POST y PUT el objeto que se debe adicionar en el body del consumo http debe conteler la siguiente estructura:

Objeto json
============

{
  "inmueble": {
    "tipo": "string",
    "subtipo": "string",
    "general": {
      "direccion": "string",
      "ciudad": "string",
      "departamento": "string",
      "pais": "string",
      "telefono": "string"
    },
    "interior": {
      "cuartos": "float",
      "baños": "integer",
      "closets": "integer",
      "calentador": "boolean"
    },
    "exterior": {
      "vigilancia": "string",
      "parqueadero": "string",
      "salon_social": "boolean",
      "numero_pisos": "integer"
    }
  }
}
